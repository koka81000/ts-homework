interface coordinate {
    x: number
    y: number
}

const coords: coordinate[] = [];
const canvas: HTMLCanvasElement = document.getElementById('canvas') as HTMLCanvasElement;
const context: CanvasRenderingContext2D = canvas.getContext('2d');
const resetButon: HTMLElement = document.querySelector('.reset')
let coordinate: { x: number, y: number };
let areaField: HTMLElement = document.getElementById('area')
let perimeterField: HTMLElement = document.getElementById('perimeter')


function getCoords(event: MouseEvent) {
    coordinate = {x: event.x, y: event.y};
    coords.push(coordinate);
    const max: number = coords.length - 1;
    if (coords[max - 1] !== undefined) {
        const currentCoord: coordinate = coords[max];
        const previosCoord: coordinate = coords[max - 1];
        context.beginPath();
        context.moveTo(previosCoord.x, previosCoord.y);
        context.lineTo(currentCoord.x, currentCoord.y);
        context.stroke();
    }
}

function getArea(coords: coordinate[]) {
    let area: number = 0;
    let j = coords.length - 1;
    for (let i: number = 0; i < coords.length; i++) {
        area += coords[i].x * coords[j].y - coords[i].y * coords[j].x;
        j = i;
    }
    area = Math.abs(area / 2);
    areaField.innerHTML = `Area: ${Math.trunc(area)}`
}

function getPerimeter(coords: coordinate[]) {
    let perimeter: number = 0;
    const coordsCopy: { x: number, y: number }[] = [...coords];
    coordsCopy.push(coordsCopy[0]);
    for (let k: number = 0; k < coordsCopy.length - 1; k++) {
        let xDiff: number = coordsCopy[k + 1].x - coordsCopy[k].x;
        let yDiff: number = coordsCopy[k + 1].y - coordsCopy[k].y;
        perimeter = perimeter + Math.pow(xDiff * xDiff + yDiff * yDiff, 0.5);
    }
    perimeterField.innerHTML = `Perimeter: ${Math.trunc(perimeter)}`
} 

canvas.addEventListener('click', (event)=>getCoords(event));
resetButon.addEventListener('click', onResset)


canvas.addEventListener('mouseleave', () => {
    if (coords.length > 1) {
        context.lineTo(coords[0].x, coords[0].y);
        context.stroke();
        getArea(coords);
        getPerimeter(coords);
        
    }
});

function onResset(){
    window.location.reload()    
}



