var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var coords = [];
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var resetButon = document.querySelector('.reset');
var coordinate;
var areaField = document.getElementById('area');
var perimeterField = document.getElementById('perimeter');
function getCoords(event) {
    coordinate = { x: event.x, y: event.y };
    coords.push(coordinate);
    var max = coords.length - 1;
    if (coords[max - 1] !== undefined) {
        var currentCoord = coords[max];
        var previosCoord = coords[max - 1];
        context.beginPath();
        context.moveTo(previosCoord.x, previosCoord.y);
        context.lineTo(currentCoord.x, currentCoord.y);
        context.stroke();
    }
}
function getArea(coords) {
    var area = 0;
    var j = coords.length - 1;
    for (var i = 0; i < coords.length; i++) {
        area += coords[i].x * coords[j].y - coords[i].y * coords[j].x;
        j = i;
    }
    area = Math.abs(area / 2);
    areaField.innerHTML = "Area: " + Math.trunc(area);
}
function getPerimeter(coords) {
    var perimeter = 0;
    var coordsCopy = __spreadArrays(coords);
    coordsCopy.push(coordsCopy[0]);
    for (var k = 0; k < coordsCopy.length - 1; k++) {
        var xDiff = coordsCopy[k + 1].x - coordsCopy[k].x;
        var yDiff = coordsCopy[k + 1].y - coordsCopy[k].y;
        perimeter = perimeter + Math.pow(xDiff * xDiff + yDiff * yDiff, 0.5);
    }
    perimeterField.innerHTML = "Perimeter: " + Math.trunc(perimeter);
}
canvas.addEventListener('click', function (event) { return getCoords(event); });
resetButon.addEventListener('click', onResset);
canvas.addEventListener('mouseleave', function () {
    if (coords.length > 1) {
        context.lineTo(coords[0].x, coords[0].y);
        context.stroke();
        getArea(coords);
        getPerimeter(coords);
    }
});
function onResset() {
    window.location.reload();
}
